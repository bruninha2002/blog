<DOCTYPE html>
<html> 
	<head>
	<meta name="charset" content="utf-8" />
	<meta name="viewport" content="width-device-width, initial-scale-1.0"/>
	
	<title> Auto-estima </title>
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>

	</head>
	<body>
	<header>
		<div class="row flex-nowrap justify-content-between align-items-center">
      <div class="col-4 pt-1">
        <a class="btn btn-sm btn-outline-secondary" href="#">Sobre o blog</a>
      </div>
      <div class="col-4 text-center">
        <a class="blog-header-logo text-dark" href="#"><img src= "imagens/logo.png"/></a>
      </div>
      <div class="col-4 d-flex justify-content-end align-items-center">
        <a class="text-muted" href="#">
          <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="mx-3" role="img" viewBox="0 0 24 24" focusable="false"><title>Search</title><circle cx="10.5" cy="10.5" r="7.5"></circle><path d="M21 21l-5.2-5.2"></path></svg>
        </a>
        <a class="btn btn-sm btn-outline-secondary" href="#">Sign up</a>
      </div>
    </div>
    <hr style="height:2px; border:none; color:#000; background-color:#000; margin-top: 0px; margin-bottom: 0px;"/>
	</header>
	<div class="nav-scroller py-1 mb-2">
    <nav class="nav d-flex justify-content-between">
      <a class="p-2 text-muted "  href="#">Cabelos</a>
      <a class="p-2 text-muted" href="#">Unhas</a>
      <a class="p-2 text-muted" href="#">Pele</a>
      <a class="p-2 text-muted" href="#">Maquiagem</a>
      <a class="p-2 text-muted" href="#">Dicas</a>
      <a class="p-2 text-muted" href="#">Ideas</a>
      <a class="p-2 text-muted" href="#">Auto-estima</a>
  
    </nav>
  </div>
  <hr style="height:2px; border:none; color:#000; background-color:#000; margin-top: 0px; margin-bottom: 0px;"/>

  <div class="jumbotron p-4 p-md-5 text-white ">
    <div class="col-md-6 px-0">
      <h1 class="display-4 font-italic">Title of a longer featured blog post</h1>
      <p class="lead my-3">Multiple lines of text that form the lede, informing new readers quickly and efficiently about what’s most interesting in this post’s contents.</p>
      <p class="lead mb-0"><a href="#" class="text-white font-weight-bold">Continue reading...</a></p>
    </div>
	</div>
	
	{% for post in object_list %}
	<h4>{{post}}</h4>
	<p>{{post.description}}</p>
	<b> Autor :</b> {{ post.author }}
	<p>{{post.text}}</p>
  {% endfor %}

		
	</body>
	<style>
		.jumbotron {
			background-image : url(imagens/fundo.png);
		}
		.blog{
			font-size : 24px;
			font-family : "long";
			margin-left : 25px;

		}
		.p-2 {
			font-size : 22px;
			font-family : "long";
			color : #ad000a;
		}
		.nav-scroller {
			margin-left : 28px;
			margin-right : 28px;
			color : #ad000a ;
		}
		.btn {
			margin-right : 28px;
			margin-left : 28px;
			font-size : 24px;
			font-family : "long";
		}
	</style>
</html>